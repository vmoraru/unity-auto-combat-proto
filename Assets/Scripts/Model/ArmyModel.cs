﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ArmyModel {

	public ObservableValue<string> Name = new ObservableValue<string>();
	public ObservableList<UnitModel> Units = new ObservableList<UnitModel>();

	public ArmyModel(String name, List<UnitModel> units) {
		this.Name.value = name;
		this.Units.value = units;
	}

}
