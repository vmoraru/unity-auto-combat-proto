﻿using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ObservableValue<T> {

	private T _value;
	public T value { 
		get { return _value; } 
		set { this._value = value; NotifyChange (); } 
	}

	public delegate void Change(T value);
	public event Change OnChange;

	public ObservableValue(T initialValue) {
		this.value = initialValue;
	}

	public ObservableValue() {}

	private void NotifyChange() {
		if (OnChange != null) {
			OnChange (this._value);
		}
	}

}
