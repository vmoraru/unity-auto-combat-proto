﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RecruitmentModel {

	public ArmyModel PlayerArmy;
	public ObservableList<UnitModel> AvailableUnits;

	public RecruitmentModel(ArmyModel army, ObservableList<UnitModel> AvailableUnits) {
		this.PlayerArmy = army;
		this.AvailableUnits = AvailableUnits;
	}

}

