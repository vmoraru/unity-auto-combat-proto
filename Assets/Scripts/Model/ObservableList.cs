﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObservableList<T> {

	public delegate void Change(List<T> value);
	public event Change OnChange;

	public delegate void AddEvent(T item);
	public event AddEvent OnAdd;

	public delegate void RemoveEvent(T item);
	public event RemoveEvent OnRemove;

	private List<T> _value = new List<T>();
	public List<T> value { 
		get { return _value; }
		set {
			this._value = value;
			NotifyChange ();
		}
	}

	public void Add(T item) {
		_value.Add (item);
		if (OnAdd != null) {
			OnAdd (item);
		}
	}

	public void Remove(T item) {
		_value.Remove (item);
		if (OnRemove != null) {
			OnRemove (item);
		}
	}

	public void RemoveAll(System.Predicate<T> p) {
		List<T> found = _value.FindAll (p);
		found.ForEach (t => Remove(t));
	}

	private void NotifyChange() {
		if (OnChange != null) {
			OnChange (this._value);
		}
	}

}
