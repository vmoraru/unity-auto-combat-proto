﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BattleOutcome {
	
	PLAYER_WINS, ENEMY_WINS, NONE

}

