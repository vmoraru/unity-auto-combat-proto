﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleModel {

	public ArmyModel PlayerArmy;
	public ArmyModel EnemyArmy;
	public ObservableValue<int> Turn;
	public ObservableValue<BattleOutcome> Outcome;

	public BattleModel(ArmyModel playerArmy, ArmyModel enemyArmy) {
		this.PlayerArmy = playerArmy;
		this.EnemyArmy = enemyArmy;
		this.Turn = new ObservableValue<int> (0);
		this.Outcome = new ObservableValue<BattleOutcome> (BattleOutcome.NONE);
	}

	public void DoBattleEnd() {
		PlayerArmy.Units.RemoveAll (u => u.Dead);
		PlayerArmy.Units.value.ForEach (u => u.Refresh ());
	}

}
