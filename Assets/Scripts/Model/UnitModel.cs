﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class UnitModel {

	public delegate void Killed(UnitModel unit);
	public event Killed OnKilled;

	public delegate void Attack();
	public event Attack OnAttack;

	public delegate void DamageTaken(int amount);
	public event DamageTaken OnDamageTaken;

	public delegate void ChangeAttackOrder(int order);
	public event ChangeAttackOrder OnAttackOrderChanged;

	public bool Dead { get; set; }
	public ObservableValue<int> hp = new ObservableValue<int>();
	public ObservableValue<int> maxHp = new ObservableValue<int>();
	public ObservableValue<int> level = new ObservableValue<int>();
	public ObservableValue<LevelUp> levelUp = new ObservableValue<LevelUp> ();
	public ObservableValue<int> xp = new ObservableValue<int>();
	public ObservableValue<string> name = new ObservableValue<string>();
	public ObservableValue<int> dmg = new ObservableValue<int>();
	public ObservableValue<int> armor = new ObservableValue<int> ();
	public ObservableValue<int> initiative = new ObservableValue<int> ();
	public UnitType UnitType;

	private XpReward xpReward;

	public static Predicate<UnitModel> DEAD = u => u.Dead;
	public static Predicate<UnitModel> NOT_DEAD = u => !DEAD.Invoke (u);

	public UnitModel(UnitType type) {
		this.maxHp.value = type.baseHP;
		this.hp.value = type.baseHP;
		this.level.value = 1;
		this.xpReward = XpReward.STANDARD;
		this.levelUp.value = new LevelUp (10, 2, 1);
		this.xp.value = 0;
		this.name.value = type.name.ToString();
		this.dmg.value = type.baseDmg;
		this.armor.value = type.baseArmor;
		this.initiative.value = type.baseInitiative;
		this.UnitType = type;
	}

	public void TakeDamage(int value) {
		int newValue = Math.Max (0, (hp.value - value));
		hp.value = newValue;

		if (OnDamageTaken != null) {
			OnDamageTaken (value);
		}

		if (newValue == 0 && OnKilled != null) {
			Dead = true;
			OnKilled (this);
		} else {
			GainXp (xpReward.Defend);
		}
	}

	public int DoAttack() {
		if (OnAttack != null) {
			OnAttack ();
		}
		GainXp (xpReward.Attack);

		return this.dmg.value;
	}

	public void HandleUnitKilled(UnitModel unit) {
		GainXp ( xpReward.Kill );
	}

	public void HandleChangedAttackOrder(int order) {
		if (OnAttackOrderChanged != null) {
			OnAttackOrderChanged (order);
		}
	}

	public void Refresh() {
		GainXp (xpReward.Survive);

		if (this.xp.value >= levelUp.value.Xp) {
			// perform level up
			xp.value = 0;
			level.value += 1;
			maxHp.value += levelUp.value.HealthReward;
			dmg.value += levelUp.value.DamageReward;
		}

		this.hp.value = this.maxHp.value;
	}

	private void GainXp(int value) {
		int newValue = this.xp.value + value;
		this.xp.value = Math.Min(levelUp.value.Xp, newValue);
	}

}
