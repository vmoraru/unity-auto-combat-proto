﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncounterModel {

	public EncounterSize Size { get; private set; }
	public EncounterFaction Faction { get; private set; }
	public string Name { get { return this.ToString (); } }

	public EncounterModel(EncounterSize size, EncounterFaction faction) {
		this.Size = size;
		this.Faction = faction;
	}

	public override string ToString ()
	{
		return string.Format ("{0} of {1}", Size, Faction);
	}

}
