﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EncounterFaction {

	LOCALS,
	GUARDS,
	LORD_ARMY,
	LIGHT_COUNCIL

}
