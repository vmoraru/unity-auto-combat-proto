﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EncounterSize {

	SINGLE, // 1
	COUPLE, // 2
	FEW, // 3-5
	SEVERAL, // 5-9
	PACK, // 10-15
	ARMY, // 15-20

}
