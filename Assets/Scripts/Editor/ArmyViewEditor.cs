﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ArmyView))]
public class ArmyViewEditor : Editor {

	private ArmyView armyView;

	void OnEnable () {
		armyView = ((ArmyView)target);
	}

	public override void OnInspectorGUI() {
		DrawDefaultInspector ();

		if (GUILayout.Button ("redraw", null)) {
			armyView.DrawUnitGrid ();
		}
	}
}
