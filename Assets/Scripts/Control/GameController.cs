﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public BattleController BattleController;
	public StrategyController StrategyController;

	private EnemyFactory enemyFactory;
	private Player player;

	// Use this for initialization
	void Start () {
		BattleController.OnClosed += BattleController_OnClosed;
		StrategyController.OnEncounterOpen += StrategyController_OnEncounterOpen;

		this.enemyFactory = GameObject.FindObjectOfType<EnemyFactory> ();
		this.player = GameObject.FindObjectOfType<Player> ();
	}

	void StrategyController_OnEncounterOpen (EncounterModel encounter)
	{
		StrategyController.Hide ();
		ArmyModel enemyArmy = enemyFactory.Create (encounter);
		BattleModel model = new BattleModel (player.Army, enemyArmy);

		BattleController.SetModel (model);
		BattleController.StartBattle ();
	}

	void BattleController_OnClosed (BattleModel battle)
	{
		StrategyController.Show ();
	}

}
