﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof (BattleView))]
[RequireComponent(typeof (BattleStats))]
public class BattleController : MonoBehaviour {

	public delegate void Closed(BattleModel battle);
	public static event Closed OnClosed;

	public BattleStats Stats;
	public BattleView View;
	public BattleModel Battle;

	private List<UnitModel> attackOrder = new List<UnitModel> ();
	private List<UnitModel> PlayerAliveUnits;
	private List<UnitModel> EnemyAliveUnits;

	public void StartBattle() {
		View.Show ();
		InitAttackOrder ();
	}

	public void SetModel(BattleModel battle) {
		this.Battle = battle;
		this.View.SetModel (battle);
		this.Stats.Init (battle.PlayerArmy.Units.value, battle.EnemyArmy.Units.value);

		this.PlayerAliveUnits = battle.PlayerArmy.Units.value.FindAll (UnitModel.NOT_DEAD);
		this.EnemyAliveUnits = battle.EnemyArmy.Units.value.FindAll (UnitModel.NOT_DEAD);
	}
		
	public void PerformNextAttack() {
		if (Battle.Outcome.value != BattleOutcome.NONE) {
			Debug.LogWarning ("Cannot advance battle when in state: " + Battle.Outcome.value);
			return;
		}
			
		UnitModel attacker = attackOrder [0];
		RemoveUnitFromAttackOrder (attacker);

		UnitModel defender = GetVictim (attacker);

		ExecuteAttack (attacker, defender);

		// if all moves have been depleeted, reinitialize attack order
		if (attackOrder.Count == 0) {
			InitAttackOrder ();
		}
		// after each attack, notify an update of the turn ordering
		UpdateTurnOrders ();
	}

	private void ExecuteAttack(UnitModel attacker, UnitModel defender) {
		int damage = attacker.DoAttack ();
		Stats.ReportDmgDealt (attacker, damage);
		defender.TakeDamage (damage);

		if (defender.Dead) {
			Stats.ReportKilledUnit (attacker);
			HandleUnitKilled (defender);
		}
	}

	private void InitAttackOrder() {
		attackOrder.Clear ();
		attackOrder.AddRange ( PlayerAliveUnits );
		attackOrder.AddRange ( EnemyAliveUnits );

		// sort by initiative, scramble same initiative units
		var query = from unit in attackOrder select new  { Unit = unit, Order = Random.value};
		Debug.Log (query.Count());
		attackOrder = query.OrderByDescending (o => o.Unit.initiative.value).ThenBy (o => o.Order).Select (o => o.Unit).ToList();
		UpdateTurnOrders ();
	}

	void HandleUnitKilled (UnitModel unit)
	{
		// remove unit from attack order
		RemoveUnitFromAttackOrder(unit);

		bool removed = PlayerAliveUnits.Remove (unit);
		if (removed && PlayerAliveUnits.Count == 0) {
			EndBattle (BattleOutcome.ENEMY_WINS);
		}

		removed = EnemyAliveUnits.Remove (unit);
		if (removed && EnemyAliveUnits.Count == 0) {
			EndBattle (BattleOutcome.PLAYER_WINS);
		}
	}

	private UnitModel GetVictim(UnitModel attacker) {
		List<UnitModel> potentialVictims;

		// TODO this is a really naive ownership check
		if (Battle.PlayerArmy.Units.value.Contains (attacker)) {
			potentialVictims = EnemyAliveUnits;
		} else {
			potentialVictims = PlayerAliveUnits;
		}

		return potentialVictims [Random.Range (0, potentialVictims.Count)];
	}

	private void EndBattle(BattleOutcome outcome) {
		Battle.Outcome.value = outcome;
		Stats.CalculateEndStats (Battle.PlayerArmy.Units.value, Battle.EnemyArmy.Units.value);
	}

	private void RemoveUnitFromAttackOrder(UnitModel unit) {
		attackOrder.Remove (unit);
		unit.HandleChangedAttackOrder (-1);
	}

	private void UpdateTurnOrders() {
		Enumerable.Range (0, attackOrder.Count).ToList ().ForEach (i => attackOrder [i].HandleChangedAttackOrder (i + 1));
	}

	public bool Ended() {
		return this.Battle.Outcome.value != BattleOutcome.NONE;
	}

	public void CloseBattle() {
		// add unit XP, levelup, clear dead units
		Battle.DoBattleEnd ();

		View.Close ();
		if (OnClosed != null) {
			OnClosed (Battle);
		}
	}
		
}
