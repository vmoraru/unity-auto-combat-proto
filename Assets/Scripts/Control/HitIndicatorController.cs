﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitIndicatorController : MonoBehaviour {

	public GameObject HitIndicatorTemplate;
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CreateHitIndicator(UnitView view, int value) {
		GameObject hit = GameObject.Instantiate (HitIndicatorTemplate);
		hit.transform.parent = this.transform;

		HitIndicator hitScript = hit.GetComponent<HitIndicator> ();
		hitScript.Init (view.gameObject, value);
	}

}
