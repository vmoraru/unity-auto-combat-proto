﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrategyController : MonoBehaviour {

	public delegate void EncounterOpen(EncounterModel encounter);
	public static event EncounterOpen OnEncounterOpen;

	public Recruitment recruitment;

	public EncounterView[] encounters;

	void Awake() {
	}

	// Use this for initialization
	void Start () {
		foreach(EncounterView e in encounters) {
			e.OnClicked += EncounterSelected;
		}
	}

	public void EncounterSelected(EncounterModel e) {
		if (OnEncounterOpen != null) {
			OnEncounterOpen (e);
		}
	}

	public void Show() {
		this.gameObject.SetActive (true);
	}

	public void Hide() {
		this.gameObject.SetActive (false);
	}

	public void ShowRecruitment() {
		recruitment.Toggle ();
	}

}
