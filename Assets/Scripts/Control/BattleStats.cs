﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStats : MonoBehaviour {

	private Dictionary<UnitModel, StatChanges> changes = new Dictionary<UnitModel, StatChanges>();

	// Use this for initialization
	void Start () {
	}

	public void Init(List<UnitModel> playerUnits, List<UnitModel> enemyUnits) {
		changes.Clear ();
		playerUnits.ForEach (u => changes.Add(u, new StatChanges (u)));
		enemyUnits.ForEach (u => changes.Add(u, new StatChanges (u)));
	}

	public void ReportKilledUnit(UnitModel who) {
		changes [who].AddUnitKilled ();
	}

	public void ReportDmgDealt(UnitModel who, int value) {
		changes [who].IncreaseDmgDealt (value);
	}

	public void CalculateEndStats(List<UnitModel> playerUnits, List<UnitModel> enemyUnits) {
		playerUnits.ForEach (u => changes [u].UpdateStats (u));
		enemyUnits.ForEach (u => changes [u].UpdateStats (u));
	}

	class StatChanges {
		public int DmgDealt { get; private set; }
		public int DmgReceived { get; private set; }
		public int Xp { get; private set; }
		public int UnitsKilled { get; private set; }

		public StatChanges(UnitModel unit) {
			this.Xp = unit.xp.value;
		}

		public void AddUnitKilled() {
			this.UnitsKilled += 1;
		}

		public void IncreaseDmgDealt(int value) {
			this.DmgDealt += value;
		}

		public void UpdateStats(UnitModel model) {
			this.Xp = model.xp.value - Xp;
			this.DmgReceived = model.maxHp.value - model.hp.value;
		}
	}

}
