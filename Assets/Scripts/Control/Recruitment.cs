﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recruitment : MonoBehaviour {

	public UnitFactory unitFactory;

	public Player player;

	public RecruitmentView View;

	// Use this for initialization
	void Start () {
		ObservableList<UnitModel> availableUnits = new ObservableList<UnitModel> ();
		availableUnits.value = BuildInitialList ();

		RecruitmentModel recruitmentModel = new RecruitmentModel (player.Army, availableUnits);
		View.SetModel (recruitmentModel);

		UnitRecruitActionsView.OnClicked += HireUnit;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Toggle() {
		if (this.gameObject.activeSelf) {
			this.gameObject.SetActive (false);
		} else {
			this.gameObject.SetActive (true);
		}
	}

	public void Show() {
		this.gameObject.SetActive (true);
	}

	private List<UnitModel> BuildInitialList() {
		List<UnitModel> units = new List<UnitModel> ();

		units.Add(unitFactory.CreateUnit (UnitArchetype.BAT));
		units.Add(unitFactory.CreateUnit (UnitArchetype.CULTIST));

		return units;
	}

	private void HireUnit(UnitModel unit) {
		ArmyModel playerArmy = player.Army;

		if (playerArmy.Units.value.Count < 20) {
			UnitModel newUnit = unitFactory.CreateUnit(unit.UnitType.name);
			player.Army.Units.Add (newUnit);
		}
	}

}
