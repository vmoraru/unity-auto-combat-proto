﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BattlePlayer : MonoBehaviour {

	private const int SPEED_PAUSED = 0;
	private const int SPEED_NORMAL = 1;
	private const int SPEED_FAST = 2;
	private const int SPEED_FASTFORWARD = 3;

	public BattleController controller;

	public Image delayProgressBar;
	public int speed;

	private Coroutine nextCoroutine;
	private DateTime coroutineStartTime;
	private DateTime coroutineScheduledTime;
	
	// Update is called once per frame
	void Update () {
		if (speed > SPEED_PAUSED) {
			if (coroutineStartTime != null && coroutineScheduledTime != null) {
				float timeSinceCoroutineStarted = (float) DateTime.Now.Subtract (coroutineStartTime).TotalSeconds;
				float fraction = timeSinceCoroutineStarted / getDelayForSpeed (speed);
				delayProgressBar.fillAmount = fraction;
			}
		}
	}

	public void SetSpeed(int value) {
		if (value < SPEED_PAUSED || value > SPEED_FASTFORWARD) {
			Debug.LogError ("Invalid speed in BattlePlayer" + value);
		}
		this.speed = value;

		// stop
		StopPlaying();

		if (value > SPEED_PAUSED) {
			// play
			nextCoroutine = StartCoroutine(NextStep());
		}
	}

	public void Next() {
		StopPlaying ();
		this.speed = SPEED_PAUSED;
		controller.PerformNextAttack ();
	}

	private IEnumerator NextStep() {
		// controller.PerformNextAttack ();
		float delay = getDelayForSpeed (speed);

		while (speed > SPEED_PAUSED && !controller.Ended()) {
			coroutineStartTime = DateTime.Now;
			coroutineScheduledTime = DateTime.Now.Add (TimeSpan.FromSeconds (delay));

			yield return new WaitForSeconds (delay);
			controller.PerformNextAttack ();
		}

	}

	private void StopPlaying() {
		if (nextCoroutine != null) {
			StopCoroutine (nextCoroutine);
			delayProgressBar.fillAmount = 0f;
		}
	}

	private float getDelayForSpeed(int speed) {
		float result = 1f;
		switch (speed) {
		case SPEED_NORMAL:
			result = 1f;
			break;
		case SPEED_FAST:
			result = .5f;
			break;
		case SPEED_FASTFORWARD:
			result = .1f;
			break;
		}
		return result;
	}

}
