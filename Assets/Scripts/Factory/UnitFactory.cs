﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitFactory : MonoBehaviour {

	private UnitDefinitions definitions;

	// Use this for initialization
	void Awake () {
		definitions = GameObject.FindObjectOfType<UnitDefinitions> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public UnitModel CreateUnit(UnitArchetype type) {
		Debug.Log ("Unit archetype " + type);
		UnitType unitType = definitions.ByArchetype (type);

		return new UnitModel (unitType);
	}

}
