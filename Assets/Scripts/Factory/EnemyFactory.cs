﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : MonoBehaviour {

	private UnitArchetype[] potentialArchetypes = { UnitArchetype.PEASANT, UnitArchetype.ARCHER };

	private UnitFactory unitFactory;

	// Use this for initialization
	void Awake () {
		this.unitFactory = GameObject.FindObjectOfType<UnitFactory> ();
	}
	
	public ArmyModel Create(EncounterModel encounter) {
		Debug.Log ("Create army for " + encounter);
		List<UnitModel> units = GenerateUnits (encounter);

		return new ArmyModel(encounter.Name, units);
	}

	private List<UnitModel> GenerateUnits(EncounterModel model) {
		int armySize = GetArmySize (model.Size);
		List<UnitModel> units = new List<UnitModel>();
		for (int i = 0; i < armySize; i++) {
			units.Add (GetUnit ());
		}
		return units;
	}

	private UnitModel GetUnit() {
		UnitArchetype type = potentialArchetypes [Random.Range (0, potentialArchetypes.Length)];
		return unitFactory.CreateUnit (type);
	}

	private int GetArmySize(EncounterSize size) {
		int result = 1;

		switch (size) {
		case EncounterSize.SINGLE:
			result = 1;
			break;
		case EncounterSize.COUPLE:
			result = 2;
			break;
		case EncounterSize.FEW:
			result = Random.Range(3, 5);
			break;
		case EncounterSize.SEVERAL:
			result = Random.Range(6, 9);
			break;
		case EncounterSize.PACK:
			result = Random.Range(10, 15);
			break;
		case EncounterSize.ARMY:
			result = Random.Range(16, 20);
			break;
		}

		return result;
	}

}
