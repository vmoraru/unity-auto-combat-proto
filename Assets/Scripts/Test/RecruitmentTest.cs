﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecruitmentTest : MonoBehaviour {

	public Recruitment recruitment;

	// Use this for initialization
	void Awake () {
		Startup.OnGameStart += () => recruitment.Show ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
