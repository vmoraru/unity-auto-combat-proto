﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmyTest : MonoBehaviour {

	public ArmyView armyView;

	// Use this for initialization
	void Start () {
		UnitFactory factory = GameObject.FindObjectOfType<UnitFactory> ();

		List<UnitModel> units = new List<UnitModel> ();
		for (int i = 0; i < 20; i++) {
			units.Add(factory.CreateUnit (UnitArchetype.CULTIST));
		}

		ArmyModel model = new ArmyModel ("Test army", units);

		armyView.SetModel (model);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
