﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitTurnOrderView : MonoBehaviour {

	public Color nextColor;
	public Color soonColor;
	public Color otherColor;

	public Sprite nextSprite;
	public Sprite soonSprite;
	public Sprite otherSprite;

	public Text order_text;
	public SpriteRenderer background;

	public void SetOrder(int order) {
		order_text.text = order.ToString ();

		Color textColor = otherColor;
		Sprite sprite = otherSprite;

		if (order <= 0) {
			order_text.text = "-";
		} else if (order == 1) {
			textColor = nextColor;
			sprite = nextSprite;
		} else if (order < 4) {
			textColor = soonColor;
			sprite = soonSprite;
		}

		order_text.color = textColor;
		background.sprite = sprite;
	}

	public void SetVisible(bool visible) {
		order_text.gameObject.SetActive (visible);
		background.gameObject.SetActive (visible);
	}
}
