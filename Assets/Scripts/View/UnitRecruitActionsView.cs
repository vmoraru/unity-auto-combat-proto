﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRecruitActionsView : MonoBehaviour, View<UnitModel> {

	public delegate void Clicked(UnitModel model);
	public static event Clicked OnClicked;

	private UnitModel model;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetModel(UnitModel model) {
		this.model = model;
	}

	public void UpdateView() {
	}

	public void Clear() {
	}

	public void HandleClicked() {
		if (OnClicked != null) {
			OnClicked (model);
		}
	}

}
