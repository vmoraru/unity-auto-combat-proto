﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridPlacement : MonoBehaviour {

	public float startOffsetX;
	public float startOffsetY;
	public float unitOffsetX;
	public float unitOffsetY;

	public static GridPlacement instance;

	// Use this for initialization
	void Awake () {
		instance = this;
	}

	public void DrawGrid(List<GameObject> objects) {
		int row;
		int col;

		for (int i = 0; i < objects.Count; i++) {
			Transform unitT = objects [i].transform;
			// reset position
			unitT.localPosition = new Vector3(startOffsetX, startOffsetY, 0f);

			row = i / 5;
			col = i % 5;

			unitT.localPosition += Vector3.right * (col * unitOffsetX);
			unitT.localPosition += Vector3.down * (row * unitOffsetY);
		}
	}
}
