﻿using System.Collections;
using System.Collections.Generic;

public interface View<T> {

	void SetModel(T model);

	void UpdateView();

	void Clear();

}
