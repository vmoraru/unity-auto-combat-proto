﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitView : MonoBehaviour, View<UnitModel> {

	private const string TRIGGER_TAKE_DMG = "damageTaken";
	private const string TRIGGER_ATTACK = "attack";
	
	public Animator animatorController;

	public SpriteRenderer spriteRendered;
	public UnitHealthView unitHealthView;
	public UnitXpView unitXpView;
	public UnitTurnOrderView turnOrderView;
	public HitIndicatorController hitIndicator;
	public Text hp_text;
	public Text dmg_text;
	public Text initiative_text;
	public Text name_text;
	public Text lvl_text;

	public UnitModel model;

	// Use this for initialization
	void Start () {
		
	}

	public virtual void Clear() {
		// need to remove all of these listeners when view is destroyed
		model.hp.OnChange -= UpdateHitpoints;
		model.name.OnChange -= UpdateName;
		model.dmg.OnChange -= UpdateDamage;
		model.initiative.OnChange -= UpdateInitiative;
		model.xp.OnChange -= UpdateXp;
		model.level.OnChange -= UpdateLevel;

		model.OnAttackOrderChanged -= Model_OnAttackOrderChanged;
		model.OnDamageTaken -= Model_OnDamageTaken;
		model.OnAttack -= Model_OnAttack;
		model.OnKilled -= Model_OnKilled;
	}

	public virtual void SetModel(UnitModel model) {
		this.model = model;

		// register change listeners
		model.hp.OnChange += UpdateHitpoints;
		model.name.OnChange += UpdateName;
		model.dmg.OnChange += UpdateDamage;
		model.initiative.OnChange += UpdateInitiative;
		model.xp.OnChange += UpdateXp;
		model.level.OnChange += UpdateLevel;

		model.OnAttackOrderChanged += Model_OnAttackOrderChanged;
		model.OnDamageTaken += Model_OnDamageTaken;
		model.OnAttack += Model_OnAttack;
		model.OnKilled += Model_OnKilled;

		UpdateView ();
	}

	void Model_OnAttack ()
	{
		animatorController.SetTrigger (TRIGGER_ATTACK);
	}

	void Model_OnDamageTaken (int amount)
	{
		animatorController.SetTrigger (TRIGGER_TAKE_DMG);
		hitIndicator.CreateHitIndicator (this, amount);
	}

	private void Model_OnAttackOrderChanged (int order)
	{
		turnOrderView.SetOrder (order);
	}

	public void UpdateView() {
		UpdateHitpoints (this.model.hp.value);
		UpdateName (model.name.value);
		UpdateDamage (model.dmg.value);
		UpdateInitiative (model.initiative.value);
		UpdateLevel (model.level.value);
		UpdateXp (model.xp.value);
		UpdateSprite (model.UnitType.baseTexture);
	}

	public void ShowTurnOrder(bool showTurnOrder) {
		this.turnOrderView.SetVisible (showTurnOrder);
	}

	void Model_OnKilled (UnitModel unit)
	{
		spriteRendered.sprite = unit.UnitType.deadTexture;
	}

	private void UpdateHitpoints(int value) {
		this.hp_text.text = value.ToString();
		this.unitHealthView.UpdateValue (value, model.maxHp.value);
	}

	private void UpdateName(string name) {
		this.name_text.text = name.ToString ();
	}

	private void UpdateXp(int value) {
		unitXpView.UpdateValue (value, model.levelUp.value.Xp);
	}

	private void UpdateDamage(int damage) {
		this.dmg_text.text = damage.ToString ();
	}

	private void UpdateInitiative(int initiative) {
		this.initiative_text.text = initiative.ToString ();
	}

	private void UpdateLevel(int value) {
		this.lvl_text.text = value.ToString ();
	}

	private void UpdateSprite(Sprite sprite) {
		spriteRendered.sprite = sprite;
	}
		
}