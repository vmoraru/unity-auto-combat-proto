﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RecruitableUnitsView : MonoBehaviour, View<ObservableList<UnitModel>> {

	public GameObject unitTemplate;
	public GameObject recruitActionsTemplate;
	public GridPlacement gridPlacement;

	public ObservableList<UnitModel> UnitList;

	public Transform frame;

	private List<UnitView> unitViews = new List<UnitView> ();

	public void Clear() {
		// TODO
		GameObject.Destroy (this.gameObject);
	}
	
	public void SetModel(ObservableList<UnitModel> model) {
		this.UnitList = model;

		// TODO

		UpdateView ();
	}

	public void UpdateView() {
		UnitList.value.ForEach (unit => this.unitViews.Add(CreateUnitView(unit)) );
		gridPlacement.DrawGrid (unitViews.Select (unit => unit.gameObject).ToList());
	}

	private UnitView CreateUnitView(UnitModel model) {
		GameObject unitObject = GameObject.Instantiate (unitTemplate);
		unitObject.transform.parent = this.frame;

		GameObject actionsObject = GameObject.Instantiate (recruitActionsTemplate);
		actionsObject.transform.parent = unitObject.transform;

		UnitView unitView = unitObject.GetComponent<UnitView> ();
		unitView.ShowTurnOrder (false);
		unitView.SetModel (model);

		UnitRecruitActionsView recruitActions =  actionsObject.GetComponent<UnitRecruitActionsView> ();
		recruitActions.SetModel (model);

		return unitView;
	}

}
