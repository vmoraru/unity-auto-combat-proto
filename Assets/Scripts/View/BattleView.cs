﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleView : MonoBehaviour, View<BattleModel> {

	public BattleModel Battle;
	public Text status_txt;
	public RectTransform play_pnl;
	public RectTransform end_pnl;

	public ArmyView playerArmyView;
	public ArmyView enemyArmyView;

	// Use this for initialization
	void Start () {
		status_txt.text = "";
	}

	public void Clear() {
		// GameObject.Destroy (this.gameObject);
	}

	public void SetModel(BattleModel combat) {
		this.Battle = combat;

		// TODO unregister events

		// TODO register events
		if (combat != null) {
			combat.Outcome.OnChange += OnCombatEnded;
		}

		UpdateView ();
	}

	void OnCombatEnded (BattleOutcome value)
	{
		status_txt.text = value.ToString ();
		end_pnl.gameObject.SetActive (true);
		play_pnl.gameObject.SetActive (false);
	}

	public void UpdateView() {
		this.playerArmyView.SetModel(Battle.PlayerArmy);
		this.enemyArmyView.SetModel (Battle.EnemyArmy);

		this.status_txt.text = "";
	}

	public void Close() {
		this.gameObject.SetActive (false);
		// TODO unregister and destroy armies and stuff
		// GameObject.Destroy(playerArmyView.gameObject);
		// GameObject.Destroy (enemyArmyView.gameObject);
	}

	public void Show() {
		end_pnl.gameObject.SetActive (false);
		play_pnl.gameObject.SetActive (true);
		this.gameObject.SetActive (true);
	}

}
