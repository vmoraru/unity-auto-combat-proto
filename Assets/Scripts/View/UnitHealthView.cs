﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitHealthView : MonoBehaviour {

	public Color midColor;
	public Color lowColor;
	private Color fullColor = Color.white;

	public Image HealthBar;

	public void UpdateValue(int currentValue, int maxValue) {
		float fraction = (float)currentValue / maxValue;

		Color hpColor = fullColor;
		if (fraction < .4f) {
			hpColor = lowColor;
		} else if (fraction < .7f) {
			hpColor = midColor;
		}

		HealthBar.fillAmount = fraction;
		HealthBar.color = hpColor;
	}

}
