﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EncounterView : MonoBehaviour, View<EncounterModel> {
	
	public delegate void Clicked(EncounterModel encounter);
	public event Clicked OnClicked;

	public Color easy_color;
	public Color medium_color;
	public Color hard_color;

	public Text difficulty_txt;
	public SpriteRenderer spriteRenderer;

	public EncounterFaction faction;
	public EncounterSize size;

	public EncounterModel Model { get; set; }

	// Use this for initialization
	void Start () {
		SetModel (new EncounterModel (size, faction));
	}

	public void Clear() {
		GameObject.Destroy (this.gameObject);
	}

	public void SetModel(EncounterModel model) {
		this.Model = model;

		UpdateView ();
	}

	public void UpdateView() {
		difficulty_txt.text = faction.ToString () + " - " + size.ToString ();
		Debug.Log (difficulty_txt.text);
		Color diffColor = Color.white;

		// TODO

		spriteRenderer.color = diffColor;
	}

	void OnMouseDown() {
		if (OnClicked != null) {
			OnClicked (Model);
		}
	}

}
