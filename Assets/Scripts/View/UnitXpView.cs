﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitXpView : MonoBehaviour {

	public Image XpBar;

	public void UpdateValue(int currentValue, int maxValue) {
		XpBar.fillAmount = (float) currentValue / maxValue;
	}

}
