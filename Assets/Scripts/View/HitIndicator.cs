﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitIndicator : MonoBehaviour {

	public Text hitValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Init(GameObject obj, int value) {
		hitValue.text = value.ToString ();
		this.transform.position = obj.transform.position;
	}

	public void Destroy() {
		GameObject.Destroy (this.gameObject);
	}
		
}
