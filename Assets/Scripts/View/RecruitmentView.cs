﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RecruitmentView : MonoBehaviour, View<RecruitmentModel> {

	public ArmyView PlayerArmyView;
	public RecruitableUnitsView RecruitableView;

	public RecruitmentModel Model;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Clear() {
		PlayerArmyView.Clear ();
		RecruitableView.Clear ();
	}

	public void SetModel (RecruitmentModel model) {
		this.Model = model;

		PlayerArmyView.SetModel (model.PlayerArmy);
		RecruitableView.SetModel (model.AvailableUnits);
	}

	public void UpdateView() {
		// TODO
	}

}
