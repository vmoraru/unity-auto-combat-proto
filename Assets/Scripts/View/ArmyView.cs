﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ArmyView : MonoBehaviour, View<ArmyModel> {

	public GameObject unitTemplate;
	public GridPlacement gridPlacement;

	public ArmyModel army;
	public Text name_text;

	public Transform frame;

	private List<UnitView> unitViews = new List<UnitView> ();

	public void Clear() {
		// unregister events
		if (army.Units != null) {
			army.Units.OnAdd -= AddUnit;
			army.Name.OnChange -= (value) => name_text.text = value;
		}

		// clear view
		unitViews.ForEach( uv => {
			uv.Clear();
			GameObject.Destroy(uv.gameObject);
		});
		unitViews.Clear ();
	}
	
	public void SetModel(ArmyModel army) {
		if (this.army != null) {
			Clear ();
		}

		this.army = army;

		// register events
		army.Units.OnAdd += AddUnit;
		army.Name.OnChange += (value) => name_text.text = value;

		UpdateView ();
	}

	public void UpdateView() {
		name_text.text = army.Name.value;
		army.Units.value.ForEach (unit => this.unitViews.Add(CreateUnitView(unit)) );
		DrawUnitGrid ();
	}

	private void AddUnit(UnitModel model) {
		unitViews.Add (this.CreateUnitView (model));
		DrawUnitGrid ();
	}

	private UnitView CreateUnitView(UnitModel model) {
		GameObject unitObject = GameObject.Instantiate (unitTemplate);
		unitObject.transform.parent = this.frame;

		UnitView unitView = unitObject.GetComponent<UnitView> ();
		unitView.SetModel (model);

		return unitView;
	}

	public void DrawUnitGrid() {
		gridPlacement.DrawGrid (this.unitViews.Select(u => u.gameObject).ToList());
	}

}
