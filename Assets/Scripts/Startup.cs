﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startup : MonoBehaviour {

	public delegate void GameStart ();
	public static event GameStart OnGameStart;

	private UnitFactory unitFactory;
	private Player player;

	void Awake () {
		this.player = GameObject.FindObjectOfType<Player> ();
		this.unitFactory = GameObject.FindObjectOfType<UnitFactory> ();
	}

	void Start() {
		StartCombatTest ();
	}

	private void StartCombatTest() {
		// Player Army
		List<UnitModel> units = new List<UnitModel>();
		units.Add (unitFactory.CreateUnit (UnitArchetype.ZOMBIE));
		units.Add (unitFactory.CreateUnit (UnitArchetype.CULTIST));
		units.Add (unitFactory.CreateUnit (UnitArchetype.BAT));
		ArmyModel playerArmy = new ArmyModel ("Your Forces", units);

		player.Army = playerArmy;

		if (OnGameStart != null) {
			OnGameStart ();
		}
	}

}
