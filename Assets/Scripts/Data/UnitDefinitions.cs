﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class UnitDefinitions : MonoBehaviour {

	public List<UnitType> UnitTypes;
	private Dictionary<UnitArchetype, UnitType> byType;

	public static UnitDefinitions Instance;

	void Awake() {
		Instance = this;

		byType = new Dictionary<UnitArchetype, UnitType> ();
		UnitTypes.ForEach (ut => byType [ut.name] = ut);
	}

	public UnitType ByArchetype(UnitArchetype type) {
		return byType [type];
	}
		
}
