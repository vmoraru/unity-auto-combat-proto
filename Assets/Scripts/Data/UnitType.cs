﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class UnitType {
	public UnitArchetype name;
	public Sprite baseTexture;
	public Sprite deadTexture;
	public int baseHP;
	public int baseDmg;
	public int baseArmor;
	public int baseInitiative;
}

public enum UnitArchetype {
	BAT, ZOMBIE, CULTIST, SKELETON_W, SKELETON_A, ARCHER, PEASANT
}
