﻿using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class XpReward {

	public static XpReward STANDARD = new XpReward (2, 1, 3, 3);

	public int Attack { get; private set; }
	public int Defend { get; private set; }
	public int Kill { get; private set; }
	public int Survive { get; private set; }

	public XpReward(int attackXp, int defendXp, int killXp, int surviveXp) {
		this.Attack = attackXp;
		this.Defend = defendXp;
		this.Kill = killXp;
		this.Survive = surviveXp;
	}

}
