﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp {

	public int Xp { get; private set; }
	public int HealthReward { get; private set; }
	public int DamageReward { get; private set; }

	public LevelUp(int xp, int healthReward, int dmgReward) {
		this.Xp = xp;
		this.HealthReward = healthReward;
		this.DamageReward = dmgReward;
	}

}
