# unity-auto-combat-proto

Prototype of a game relying on auto-combat mechanics similar to Jagged Alliance 2's "auto-resolve":
![](bb-readme/ja2-auto-resolve.jpg)

Looks like this:
![](bb-readme/proto-quick-combat.JPG)